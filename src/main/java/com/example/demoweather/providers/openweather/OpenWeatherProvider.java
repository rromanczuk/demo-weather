package com.example.demoweather.providers.openweather;

import com.example.demoweather.dto.TemperatureUnit;
import com.example.demoweather.dto.Weather;
import com.example.demoweather.providers.WeatherProvider;
import com.example.demoweather.providers.openweather.dto.TemperatureSystem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OpenWeatherProvider implements WeatherProvider {

    private final OpenWeatherClient client;

    @Override
    public Weather getWeatherForCityUsingUnit(TemperatureUnit unit, String city) {
        TemperatureSystem openWeatherUnit = TemperatureSystem.lookupByUnit(unit.toString());

        com.example.demoweather.providers.openweather.dto.Weather openWeatherWeather =
                client.getWeatherForCity(city, openWeatherUnit.toString().toLowerCase());

        return Weather.builder()
                      .temperature(openWeatherWeather.getMain().getTemp())
                      .build();
    }
}
