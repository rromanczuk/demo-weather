package com.example.demoweather.providers.openweather.dto;

import lombok.Data;

@Data
public class Weather {

    @Data
    public static class Main {
        private double temp;
    }

    private Main main;
}
