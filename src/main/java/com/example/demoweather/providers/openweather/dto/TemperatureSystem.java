package com.example.demoweather.providers.openweather.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Getter
public enum TemperatureSystem {
    STANDARD("kelvin"), METRIC("celsius"), IMPERIAL("fahrenheit");

    private String unit;

    private static final Map<String, TemperatureSystem> unitIndex = new HashMap<>(TemperatureSystem.values().length);

    static {
        for (TemperatureSystem system : TemperatureSystem.values()) {
            unitIndex.put(system.getUnit(), system);
        }
    }

    public static TemperatureSystem lookupByUnit(String name) {
        return unitIndex.get(name.toLowerCase());
    }
}
