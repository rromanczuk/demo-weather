package com.example.demoweather.providers.openweather;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("weather.providers.openweather")
@Getter
@Setter
public class OpenWeatherProperties {
    private String apiKey;
}
