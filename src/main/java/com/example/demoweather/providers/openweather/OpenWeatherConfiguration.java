package com.example.demoweather.providers.openweather;

import feign.RequestInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

@RequiredArgsConstructor
public class OpenWeatherConfiguration {

    private static final String API_KEY_REQUEST_PARAM = "appid";

    private final OpenWeatherProperties properties;

    @Bean
    public RequestInterceptor includeApiKey() {
        return template -> template.query(API_KEY_REQUEST_PARAM, properties.getApiKey());
    }
}
