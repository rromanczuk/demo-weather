package com.example.demoweather.providers.openweather;

import com.example.demoweather.providers.openweather.dto.Weather;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${weather.providers.openweather.name}", url = "${weather.providers.openweather.url}",
        configuration = OpenWeatherConfiguration.class)
public interface OpenWeatherClient {

    @GetMapping(value = "/data/2.5/weather?q={city}&units={unit}")
    Weather getWeatherForCity(@PathVariable String city, @PathVariable String unit);
}
