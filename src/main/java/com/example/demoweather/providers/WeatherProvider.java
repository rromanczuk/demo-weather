package com.example.demoweather.providers;

import com.example.demoweather.dto.TemperatureUnit;
import com.example.demoweather.dto.Weather;

public interface WeatherProvider {
    Weather getWeatherForCityUsingUnit(TemperatureUnit unit, String city);
}
