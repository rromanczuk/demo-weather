package com.example.demoweather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class DemoWeatherApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWeatherApplication.class, args);
    }

}
