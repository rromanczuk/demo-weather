package com.example.demoweather;

import com.example.demoweather.dto.TemperatureUnit;
import com.example.demoweather.dto.Weather;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/weather")
@RequiredArgsConstructor
class WeatherController {

    private final WeatherService service;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/city/{city}")
    public Weather getWeatherByCity(@RequestParam String unit, @PathVariable String city) {
        try {
            return service.getWeatherByCity(TemperatureUnit.valueOf(unit.toUpperCase()), city);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(
                    "Bad unit. Possible units are: " + Arrays.toString(TemperatureUnit.values()), e);
        }
    }
}
