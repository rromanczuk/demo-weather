package com.example.demoweather.dto;

public enum TemperatureUnit {
    CELSIUS, KELVIN, FAHRENHEIT
}
