package com.example.demoweather.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Weather {
    private double temperature;
}
