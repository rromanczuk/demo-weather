package com.example.demoweather;

import com.example.demoweather.dto.TemperatureUnit;
import com.example.demoweather.dto.Weather;
import com.example.demoweather.providers.WeatherProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WeatherService {

    private final List<WeatherProvider> provider;

    Weather getWeatherByCity(TemperatureUnit unit, String city) {
        return provider.get(0).getWeatherForCityUsingUnit(unit, city);
    }
}
