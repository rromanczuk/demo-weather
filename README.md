## To run

In root directory:

- `./gradlew clean build`
- `docker network create demo`
- `docker-compose up`

## Endpoints

- http://localhost:9010/weather/city/{city}?unit={unit}

Where:
- city - city for which to return temperature
- unit - unit in which temperature should be returned. 
         One of: [celsius, kelvin, fahrenheit].
